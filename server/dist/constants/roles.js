"use strict";

var ROLE_BRAND = 'Brand';
var ROLE_INFLUENCER = 'Influencer';
var ROLE_ADMIN = 'Admin';
var ROLE_GUEST = 'Guest';
module.exports = {
  ROLE_GUEST: ROLE_GUEST,
  ROLE_BRAND: ROLE_BRAND,
  ROLE_INFLUENCER: ROLE_INFLUENCER,
  ROLE_ADMIN: ROLE_ADMIN
};